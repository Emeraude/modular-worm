SRC =	core/main.c \
	core/plugins.c

OBJ = $(SRC:.c=.o)

CFLAGS = -W -Wall -Wextra -g3 --std=gnu99 -lrt
LDFLAGS = -ldl -lrt -Wl,--export-dynamic

all: core.out plugins

core.out: $(OBJ)
	$(CC) $(OBJ) -o core.out $(CFLAGS) $(LDFLAGS)

plugins: plugin1 plugin2 plugin3 plugin4 plugin5 plugin6 plugin7 plugin8 plugin9 plugin10 plugin11 plugin12
	mkdir -p build
	mv *.so build
	./build/cut_modules_files.rb build/*.so

plugin1:
	$(CC) -fPIC -o 1.so plugins/1_sender_plugin.c -shared $(CFLAGS)

plugin2:
	$(CC) -fPIC -o 2.so plugins/2_receiver_plugin.c -shared $(CFLAGS)

plugin3:
	$(CC) -fPIC -o 3.so plugins/3_network.c -shared $(CFLAGS)

plugin4:
	$(CC) -fPIC -o 4.so plugins/4_shellshock.c -shared $(CFLAGS)

plugin5:
	$(CC) -fPIC -o 5.so plugins/5_shellshock_command.c -shared $(CFLAGS)

plugin6:
	$(CC) -fPIC -o 6.so plugins/6_database.c -shared $(CFLAGS)

plugin7:
	$(CC) -fPIC -o 7.so plugins/7_fs.c -shared $(CFLAGS)

plugin8:
	$(CC) -fPIC -o 8.so plugins/8_discovery.c -shared $(CFLAGS)

plugin9:
	$(CC) -fPIC -o 9.so plugins/9_scanner.c -shared $(CFLAGS)

plugin10:
	$(CC) -fPIC -o 10.so plugins/10_commander.c -shared $(CFLAGS)

plugin11:
	$(CC) -fPIC -o 11.so plugins/11_message.c -shared $(CFLAGS)

plugin12:
	$(CC) -fPIC -o 12.so plugins/12_dirtycow.c -shared $(CFLAGS)

clean:
	rm -rfv $(OBJ) *.so build/*.so build/*.frag

.PHONY: all plugins clean
