#include <dlfcn.h>
#include <stdio.h>
#include <unistd.h>
#include "plugins.h"
#include "../plugins/api.h"

internal_plugin_t *plugins_g[256] = {0};

static uint32_t plugin_counter = 0;

static void set_current_plugin_id(uint8_t id) {
  plugin_counter = id;
}

uint8_t get_current_plugin_id() {
  return plugin_counter;
}

int main(void) {
  void *handle;
  void (*api_clear_read_messages)(uint8_t full);

  if ((handle = dlopen("./build/11.so", RTLD_NOW | RTLD_GLOBAL)) == NULL
      || (api_clear_read_messages = dlsym(handle, "api_clear_read_messages")) == NULL) {
    fprintf(stderr, "%s\n", dlerror());
    return 1;
  }
  api_load_plugin("./build/8.so");

  while (1) {
    for (size_t i = 1; i < sizeof plugins_g / sizeof *plugins_g; ++i) {
      if (plugins_g[i] != NULL) {
	set_current_plugin_id(plugins_g[i]->id);
        plugins_g[i]->run();
	api_clear_read_messages(0);
        usleep(100000);
      }
    }
  }
  dlclose(handle);
  return 0;
}
