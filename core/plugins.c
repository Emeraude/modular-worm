#include <dirent.h>
#include <dlfcn.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "plugins.h"
#include "../plugins/error_codes.h"

/*
  Open a plugin (shared library)

  @param path the path to the shared library to load
  @return     See error_codes.h
*/
uint32_t api_load_plugin(char const* path) {
  internal_plugin_t *plugin;
  api_plugin_t *plugin_version;

  if ((plugin = malloc(sizeof *plugin)) == NULL)
    return ERR_MEMORY;
  if ((plugin->handle = dlopen(path, RTLD_NOW)) == NULL
      || (plugin->run = dlsym(plugin->handle, "run")) == NULL
      || (plugin_version = dlsym(plugin->handle, "version")) == NULL
      || plugin_version->id > 255) {
    fprintf(stderr, "%s\n", dlerror());
    free(plugin);
    return ERR_SYSCALL;
  }
  plugin->run_instant = dlsym(plugin->handle, "run_instant");
  plugin->id = plugin_version->id;
  plugin->flag = plugin_version->flag;
  plugin->major = plugin_version->major;
  plugin->minor = plugin_version->minor;
  plugins_g[plugin->id] = plugin;
  return SUCCESS;
}

/*
  Free a loaded plugin

  @param plugin the plugin to unload
*/
void plugin_unload(internal_plugin_t *plugin) {
  dlclose(plugin->handle);
  plugins_g[plugin->id] = NULL;
  free(plugin);
}
