#pragma once

#include <stdint.h>
#include <sys/queue.h>
#include "../plugins/api.h"

typedef struct {
  unsigned id: 16;
  unsigned flag: 1;
  unsigned major: 7;
  unsigned minor: 8;
  void *handle;
  void (*run)();
  void (*run_instant)();
} internal_plugin_t;

extern internal_plugin_t *plugins_g[256];

uint32_t plugin_load(char const *path);
void plugin_unload(internal_plugin_t *plugin);
