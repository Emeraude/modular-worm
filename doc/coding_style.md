# Coding Style

## Naming

### Generic

#### Structures and typedefs
- no structure without typedef
- every typedef is **suffixed** with **_t**

### Module

#### Variable Globals
- every global in a module must be static and **prefixed** with a **p{n}_** wher en is the id of the plgin

    static int p3_server;

#### Structure and typedefs
- every typedef of a module must be prefixed with **p{n}_** where n is the id of the plugin

    typedef struct {
      unsigned id : 16;
      usigned flag: 1;
    } p3_data_t;

#### Functions
- every function of a module must be prefixed with **p{n}_** where n is the id of the plugin
  - exception: `run` and `run_instant`

    static void p3_add_state(api_msg_t *msgp, ...) { ... }
    void run() { ... }

### API

#### Function
- every function of the API is **prefixed** with **api_**

    static int api_send_message(...);

#### Structure and typedef
- every structure and typedef of the API is **prefixed** with **api_**

    typedef struct {
      unsigned stuff1 : 8;
      int stuff2: 4;
    } api_stuff_t;
