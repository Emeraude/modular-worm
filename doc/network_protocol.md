# Network protocol

Network protocol will be managed by a plugin, which means we can switch between several protocols by switching between plugins. It allows us to have both a centralized and a de-centralized worm.
Our first implementation will be the decentralized one. Here are the specs:

Since there is no central nodes, each packet will be sent over udp (stealthier, don't have to maintain a lot of connections).

However, we have to add a way to verify if the packet has been well received. If not, the remote host may be offline, so it is required to feedback the packet's transmission failure. (We might try to re-send it several times, depending on the message priority, before assuming the host is offline).

The nodes will not know the other nodes, they will be limited to a certain amount. So there will be a forward system, which is only managed by the plugin.

### Packet content (MSG)
```
magic_number: 32 bits
packet_id: 32 bits
TTL: 8 bits
destination_host: 32 bits (random id)
destination_plugin: 32 bits
body_length: 16 bits
body: 65000 bytes
```
- `body_length` is 65000 because max size of a UDP is limited to 65KiB. It is a limitation of UDP which might fragment packets over 65KB (less IPHL, UDHL and header length).
- `destination_host` is a random id assigned to each instance of the worm. Each instance will associate IDs with the hosts he knows (ip, port).
- `destination_plugin` is the identifier of the packet (id, version, ...)

### Response packet (ACK)
```
magic_number: 32 bits
packet_id: 32 bits
```

### API

- `feature`: 8 bits
  - 0x00: send message
  - 0x01: add new host
  - 0x02: list hosts
- `params`: union:
  - `message`:
    - `ttl`: 8 bits
  - `host_id`: 32 bits
    - `plugin_id`: 16 bits
    - `body_length`: 16 bits
    - `body`: void*
  - `new_host`:
    - `ip`: union
      - `ipv4`: 32 bits
      - `ipv6`: 128 bits
    - `port`: 16 bits
