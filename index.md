# Modular Worm

## Project details

- Students: Thibaut Broggi (thtb2), Arthur Poulet (agp8)
- Supervisor: Dr. Perez Delgado Carlos (C.Perez@kent.ac.uk)

## Index to Corpus

| File | Authors | Best work |
|------|---------|-----------|
| [build/cut_modules_files.rb](./build/cut_modules_files.rb) | Arthur Poulet | |
| [build/shellshock_attack_script.sh](./build/shellshock_attack_script.sh) | Arthur Poulet | |
| [core/main.c](./core/main.c) | Thibaut Broggi | |
| [core/plugins.c](./core/plugins.c) | Thibaut Broggi | |
| [core/plugins.h](./core/plugins.h) | Thibaut Broggi | |
| [Makefile](./Makefile) | All | |
| [plugins/1_sender_plugin.c](./plugins/1_sender_plugin.c) | All | |
| [plugins/2_receiver_plugin.c](./plugins/2_receiver_plugin.c) | All | |
| [plugins/3_network.c](./plugins/3_network.c) | Arthur Poulet | |
| [plugins/3_network.h](./plugins/3_network.h) | Arthur Poulet | |
| [plugins/3_network_helpers.c](./plugins/3_network_helpers.c) | Arthur Poulet | |
| [plugins/3_network_recv.c](./plugins/3_network_recv.c) | Arthur Poulet | |
| [plugins/3_network_send.c](./plugins/3_network_send.c) | Arthur Poulet | |
| [plugins/4_shellshock.c](./plugins/4_shellshock.c) | Arthur Poulet | * |
| [plugins/4_shellshock.h](./plugins/4_shellshock.h) | Arthur Poulet | |
| [plugins/5_shellshock_command.c](./plugins/5_shellshock_command.c) | Arthur Poulet | |
| [plugins/5_shellshock_command.h](./plugins/5_shellshock_command.h) | Arthur Poulet | |
| [plugins/6_database.c](./plugins/6_database.c) | Thibaut Broggi | * |
| [plugins/6_database.h](./plugins/6_database.h) | Thibaut Broggi | * |
| [plugins/7_fs.c](./plugins/7_fs.c) | Arthur Poulet | |
| [plugins/7_fs.h](./plugins/7_fs.h) | Arthur Poulet | |
| [plugins/8_discovery.c](./plugins/8_discovery.c) | Arthur Poulet, Thibaut Broggi | * |
| [plugins/9_scanner.c](./plugins/9_scanner.c) | Arthur Poulet | |
| [plugins/10_commander.c](./plugins/10_commander.c) | Arthur Poulet | |
| [plugins/10_commander.h](./plugins/10_commander.h) | Arthur Poulet | |
| [plugins/11_message.c](./plugins/11_message.c) | Thibaut Broggi | * |
| [plugins/12_dirtycow.c](./plugins/12_dirtycow.c) | Thibaut Broggi | |
| [plugins/12_dirtycow.h](./plugins/12_dirtycow.h) | Arthur Poulet | |
| [plugins/api.h](./plugins/api.h) | Thibaut Broggi | |
| [plugins/error_codes.h](./plugins/error_codes.h) | Arthur Poulet | |
| [doc/design.html](./doc/design.html) | All | |
| [doc/coding_style.html](./doc/coding_style.html) | All | |
| [doc/network_protocol.html](./doc/network_protocol.html) | All | ||
