#include "api.h"
#include "error_codes.h"
#include "12_dirtycow.h"

api_plugin_t version = {.id = 12, .flag = 0, .major = 1, .minor = 0};

int handle_incoming_messages() {
  api_msg_t msg;

  if (API_RECEIVE_MESSAGE(&((api_plugin_t){0, 0, 0, 0}), msg) == 0) {
    p12_file_edit_t *infos = (p12_file_edit_t *)(msg.content);
    if (infos->type == p12_offset) {
      /* dirty_cow_offset(infos); */
    }
    else if (infos->type == p12_match) {
      /* dirty_cow_match(infos); */
    }
    return SUCCESS;
  }
  else
    return ERR_NO_MESSAGE;
}

void run() {
  handle_incoming_messages();
}
