#pragma once

typedef enum {p12_match, p12_offset} p12_file_edit_type_e;

typedef struct {
  char filename[512];
  p12_file_edit_type_e type;
  union {
    uint32_t offset;
    struct {
      uint32_t match_size;
      char match[512];
    };
  };
  char replace[512];
  uint32_t replace_size;
} p12_file_edit_t;

#define P12_FILE_EDIT_MATCH(result, filename, match, match_size, replace, replace_size) \
  {                                                                     \
    p12_file_edit_t infos = {.type = p12_match, .match_size = match_size, .replace_size = replace_size}; \
    strncpy(infos.filename, filename, 512);                             \
    memcpy(infos.match, match, match_size);                             \
    memcpy(infos.replace, replace, replace_size);                       \
    api_send_message((api_plugin_t){12, 0, 0, 0}, &infos, sizeof(p12_file_edit_t)); \
  }

#define P12_FILE_EDIT_OFFSET(result, filename, offset, replace, replace_size) \
  {                                                                     \
    p12_file_edit_t infos = {.type = p12_offset, .offset = offset, .replace_size = replace_size}; \
    strncpy(infos.filename, filename, 512);                             \
    memcpy(infos.replace, replace, replace_size);                       \
    api_send_message((api_plugin_t){12, 0, 0, 0}, &infos, sizeof(p12_file_edit_t)); \
  }
