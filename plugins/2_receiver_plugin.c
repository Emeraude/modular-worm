#include <stdio.h>
#include "api.h"

api_plugin_t version = {.id = 2, .flag = 0, .major = 1, .minor = 0};

static void exec() {
  void *content;
  size_t length;

  api_receive_message(&((api_plugin_t){1, 0, 0, 0}), &content, &length);
  printf("[%u] Received message %p (%s) (length %lu) from %d\n", 2, content, (char *)content, length, 1);
}

void run_instant() {
  printf("[2] run_instant\n");
  exec();
}

void run() {
  printf("[2] run\n");
  exec();
}
