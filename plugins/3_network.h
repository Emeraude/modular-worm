#pragma once

#include "api.h"

// API
typedef struct {
  char host[16]; // aaa.bbb.ccc.ddd\0
  char port[6]; // abcde\0
  char content[65000];
  unsigned int destination_host;
  api_plugin_t destination_plugin;
  size_t content_length;
} p3_send_t;

#include <string.h>

#define P3_SEND(_result, _host, _port, _dest_plugin, _content, _content_length) \
  {                                                                     \
    p3_send_t payload = {.content_length = _content_length};            \
    memcpy(payload.host, _host, strlen(_host));                         \
    memcpy(payload.port, _port, strlen(_port));                         \
    memcpy(payload.content, _content, _content_length);                 \
    payload.destination_host = 0;                                       \
    payload.destination_plugin = _dest_plugin;                          \
    _result = api_send_message((api_plugin_t){3, 0, 0, 0}, &payload, sizeof(p3_send_t), 1); \
  }


// Internal stuff
#define MN_LEN      32
#define BODY_LEN    65000
#define MN_MSG      0xb1ee1e
#define MN_ACK      0xb1ee1f

/* magic number */
typedef struct {
  unsigned value: MN_LEN;
} magic_number_t;

#pragma pack(push, 1) // no mem align
/* packet with data */
typedef struct {
  magic_number_t magic_number;
  unsigned packet_id: 32;
  unsigned TTL: 8;
  unsigned destination_host: 32;
  api_plugin_t destination_plugin;
  unsigned body_length: 16;
} p3_msg_t;
#pragma pack(pop)

#pragma pack(push, 1) // no mem align
/* response (acknowledgment) without data */
typedef struct {
  magic_number_t magic_number;
  unsigned packet_id: 32;
} p3_ack_t;
#pragma pack(pop)
