
/*
  Send a packet to a given ip address.

  @param sockfd       socket to transmit on
  @param servinfo     server infos
  @param dest_host    host id
  @param dest_plugin  plugin to receive
  @param msg          buffer to transmit
  @param msg_len      amount of size in the buffer, to transmit
  @return             SUCCESS if everything is ok
*/
static int p_send_packet_to(int sockfd,
                            struct addrinfo *servinfo,
                            unsigned int dest_host,
                            api_plugin_t dest_plugin,
                            char *msg,
                            size_t msg_len) {
  int numbytes;
  char buf[sizeof(p3_msg_t) + BODY_LEN];
  p3_msg_t *packet = (void *)buf;
  char *body = buf + sizeof(p3_msg_t);
  packet->magic_number.value = MN_MSG;
  // TODO
  packet->packet_id = 0;
  packet->TTL = 255;
  packet->destination_host = dest_host;
  packet->destination_plugin = dest_plugin;
  packet->body_length = msg_len;
  p_printf_packet(packet);
  memcpy(body, msg, msg_len);
  if ((numbytes = sendto(sockfd, buf, sizeof(p3_msg_t) + packet->body_length, 0, servinfo->ai_addr, servinfo->ai_addrlen)) == -1) {
    perror("sendto");
    return ERR_WRITE;
  }
  printf("sent %d bytes to fd:%i\n", numbytes, sockfd);
  return SUCCESS;
}

/* // receive some message */
/* if (1) { // if a message has been received */
/*   char *to = "127.0.0.1"; */
/*   char *port = "1234"; */
/*   char *message = "Coco l'asticot"; */
/*   p_send_message_to(to, port, message); */
/* } */
static int p_send_message_to(p3_send_t *infos) {
  int sockfd;
  struct addrinfo hints, *servinfo;
  int rv;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  if ((rv = getaddrinfo(infos->host, infos->port, &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return ERR_SYSCALL;
  }
  if ((sockfd = socket(servinfo->ai_family,
                       servinfo->ai_socktype,
                       servinfo->ai_protocol)) == -1) {
    perror("socket");
    freeaddrinfo(servinfo);
    return ERR_SYSCALL;
  }
  if (p_send_packet_to(sockfd,
                       servinfo,
                       infos->destination_host,
                       infos->destination_plugin,
                       infos->content,
                       infos->content_length) != 0) {
    freeaddrinfo(servinfo);
    close(sockfd);
    return ERR_SYSCALL;
  }
  freeaddrinfo(servinfo);
  close(sockfd);
  return 0;
}
