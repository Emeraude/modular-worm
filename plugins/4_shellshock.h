#pragma once

#include <string.h>
#include "api.h"

typedef struct {
  uint8_t type;
  char data[511];
  // TODO: uint8_t fragment_number (0xX0 + 0x0X)
} p4_content_t;

#define P4_SHELLSHOCK_IS_DOMAIN(msg)      (((p4_content_t*)msg.content)->type == 0)
#define P4_SHELLSHOCK_IS_URL(msg)         (((p4_content_t*)msg.content)->type == 1)
#define P4_SHELLSHOCK_IS_PAYLOAD(msg)     (((p4_content_t*)msg.content)->type == 2)
#define P4_SHELLSHOCK_IS_TO_EXECUTE(msg)  (((p4_content_t*)msg.content)->type == 3)

#define P4_SHELLSHOCK_READ_DOMAIN(msg)    (((p4_content_t*)msg.content)->data)
#define P4_SHELLSHOCK_READ_URL(msg)       (((p4_content_t*)msg.content)->data)
#define P4_SHELLSHOCK_READ_PAYLOAD(msg)   (((p4_content_t*)msg.content)->data)

#define P4_SHELLSHOCK_SEND(id, datacontent, datasize)                   \
  {                                                                     \
    p4_content_t content = {.type = id, .data = {0} };                  \
    memcpy(content.data, datacontent, datasize);                        \
    api_send_message((api_plugin_t){4, 0, 0, 0}, &content, 1 + datasize, 1); \
  }

#define P4_SHELLSHOCK_SEND_DOMAIN(domain) P4_SHELLSHOCK_SEND(0, domain, strlen(domain) + 1)
#define P4_SHELLSHOCK_SEND_URL(url) P4_SHELLSHOCK_SEND(1, url, strlen(url) + 1)
#define P4_SHELLSHOCK_SEND_PAYLOAD(payload, size) P4_SHELLSHOCK_SEND(2, payload, 511)
#define P4_SHELLSHOCK_SEND_EXECUTE() api_send_message((api_plugin_t){4, 0, 0, 0}, &((p4_content_t){3, ""}), 1 + 1, 1)
