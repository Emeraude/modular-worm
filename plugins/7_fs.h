#pragma once

#include <unistd.h>

typedef struct {
  char function[256];
  void *p1;
  void *p2;
  void *p3;
  void *p4;
  void *p5;
  void *p6;
  // parameters... ?
} p7_data_send_t;

#define P7_REQUEST(req, ...)                                            \
  api_send_message_instant((api_plugin_t){7, 0, 0, 0}, &req, __VA_ARGS__);

// REPEATN(6 - COUNT_ARGS(__VA_ARGS__), 0)
#define P7_FS(result, output, name, ...)                                \
  {                                                                     \
    p7_data_send_t req = {name, __VA_ARGS__};                           \
    api_msg_t msg;                                                      \
    P7_REQUEST(req, sizeof(req), 1);                                    \
    result = API_RECEIVE_MESSAGE(&((api_plugin_t){7, 0, 0, 0}), msg);   \
    if (result == 0)                                                    \
      output = (*((FILE**)msg.content));                                \
  }

#define P7_FOPEN "fopen"
#define P7_FCLOSE "fclose"
