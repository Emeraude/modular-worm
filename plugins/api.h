#pragma once

#include <stddef.h>
#include <stdint.h>

typedef struct {
  unsigned id: 16;
  unsigned flag: 1;
  unsigned major: 7;
  unsigned minor: 8;
} api_plugin_t;

/* return 0 if success */
uint32_t api_send_message(api_plugin_t dest, void *content, size_t content_size, uint8_t priority);

/* return 0 if success */
uint32_t api_send_message_instant(api_plugin_t dest, void *content, size_t content_size, uint8_t priority);

/* return 0 if success */
/* @param from : filter to define which plugin sent the content, and then is replaced by the sender if 0 was given */
uint32_t api_receive_message(api_plugin_t *from, void **content, size_t *content_size);

/*
 * @param full 0 to remove only read messages, 1 to remove all messages
 */
void api_clear_read_messages(uint8_t full);

uint32_t api_load_plugin(char const* path);

uint8_t get_current_plugin_id();

typedef struct {
  api_plugin_t plugin;
  void *content;
  size_t content_size;
} api_msg_t;

#define API_RECEIVE_MESSAGE(from, msg) api_receive_message(from, &(msg.content), &(msg.content_size))
