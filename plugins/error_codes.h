#pragma once

#ifndef DEBUG
# define perror(...) ;
#endif

#define SUCCESS                0x00

#define ERR_SYSCALL            0x10
#define ERR_MEMORY             0x11
#define ERR_WRITE              0x12
#define ERR_READ               0x13

#define ERR_SERVER_NOT_STARTED 0x20
#define ERR_INVALID_MSG        0x21
#define ERR_INVALID_ACK        0x22
#define ERR_INVALID_HEADER     0x23
#define ERR_NO_MESSAGE         0x24

#define ERR_TEST_FAILED        0x30
#define ERR_ATTACK_FAILED      0x31
#define ERR_PLUGIN_NOT_READY   0x32

#define ERR_NOT_FOUND          0x40
#define ERR_FORBIDDEN	       0x41

#define ERR_UNKNOWN            0xff
